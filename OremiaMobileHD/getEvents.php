<?php
include_once 'lib/db_functions.php';
include_once 'lib/config.php';
include_once 'lib/CypherFunctions.php';
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
error_reporting(0);
require_once('lib/CalDav/SimpleCalDAVClient.php');
register_shutdown_function( "fatal_handler" );
date_default_timezone_set('Europe/Paris');
ini_set('memory_limit', 512000000);
$client = new SimpleCalDAVClient();
try {
	$idP = $_GET["idP"];
	$cals = isset($_GET["calendars"]) ? explode(",",$_GET["calendars"]) : array();
	$db = (new DB_Functions())->useConnexion();
	$infos = $db->getCalDAVInfos($idP);
	// print_r($properUrl);
	
	if(count($infos)==3){

		$client->connect('https://127.0.0.1:8443'.$infos[2], $infos[0], $infos[1]);
		$events = array();

		$date = DateTime::createFromFormat("Y-m-d", $_GET['date']);
		$interval = new DateInterval("P7D"); // 4 months
		$dateDeb = date_format($date->sub($interval),"Ymd")."T000000Z";
		$interval = new DateInterval("P14D");
		$dateFin = date_format($date->add($interval),"Ymd")."T000000Z";
		$client->setCalendar(new CalDavCalendar($infos[2])); 
		$events = array_merge($events,$client->getEvents($dateDeb,$dateFin)); 

		print_r( '{"results":'.$client->getClientEvents($events).'}');
	}

}catch (Exception $e) {
	echo $e->__toString();
}


function fatal_handler() {
    // header('Content-Type:text/plain'); 


    $errfile = "unknown file";
    $errstr  = "shutdown";
    $errno   = E_CORE_ERROR;
    $errline = 0;
    $error = error_get_last();
    if( $error !== NULL) {
        if($error['type'] === E_ERROR){
            http_response_code(500);
        }
        try{
            $pw = encrypt_decrypt('decrypt', PRIVATE_PDO_KEY);
            $errno   = addslashes($error["type"]);
            $errfile = addslashes($error["file"]);
            $errline = addslashes($error["line"]);
            $errstr  = addslashes($error["message"]);
            $con = new PDO('mysql:host=mysql-rdelaporte.alwaysdata.net;dbname=rdelaporte_om', "170242_robin", $pw);
            $query = "INSERT INTO bug_tracker
                      (type, file, line, message)
                    VALUES
                      ('".$errno."', '".$errfile ."', '".$errline."', '".$errstr."')
                    ON DUPLICATE KEY UPDATE
                      type     = VALUES(type)";
            $requetePrep = $con->prepare($query);
            $requetePrep->execute();


            $func = (new DB_Functions());
            $report = new Report($func);


        }catch(PDOException $e){
            //var_dump($e);
        }
       
    }

}

function pinfo() {
    ob_start();
    phpinfo();
    $data = ob_get_contents();
    ob_clean();
    return $data;
}

?>
