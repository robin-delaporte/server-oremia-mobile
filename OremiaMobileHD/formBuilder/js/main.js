var fbTemplate = document.getElementById('fb-template'),
    formContainer = document.getElementById('rendered-form'),
    $fBInstance,
    formRenderOpts,
    idDocument,
    dbname = getUrlParameter("db"),
    user = getUrlParameter("login"),
    pw = getUrlParameter("pw");
    
    
$(document).ready(function($) {
    idDocument = getUrlParameter("idDocument");
    loadExistingDocuments();
    init()
    handleAdd()
});

$(window).resize(function($) {
    fit();
});

function init(){
    if (idDocument) {
                
        query="SELECT idtype, nomtype, nomfichier FROM typedocument WHERE idtype ="+idDocument+";";
        selectQuery(query,function(data){
            if (data.results[0].nomfichier == "questionnaire medical" && data.results[0].nomtype == "questionnaire medical") {
                performUpdateFirstDoc();
            }
            var form = data.results[0].nomfichier.replace(/&amp;quot;/g, "");
            $("#fb-template").html(decodeEntities(form));
            loadForm();
        })
    }else{
        loadForm();
    }

    if(getUrlParameter("reset") == 1){
        reset();
    }
}
function loadForm(){
    $(".docSelection li").removeClass("selected");
    $("#"+idDocument+"").addClass("selected");
    $("#script_tp_relaod").remove();
    $fBInstance = $(document.getElementById('edit-form'))
    formRenderOpts = {
      container: $('form', formContainer)
    };
    reload_js("src/js/form-builder.min.js");
    var formBuilder = $(document.getElementById('fb-template')).formBuilder({messages: language,disableFields: ['autocomplete', 'hidden', 'paragraph', 'file', 'select', 'button']});
    $( ".form-builder-save").unbind( "click" );
    //$(formContainer).toggle();
    $('.form-builder-save').click(function(e) {
        e.preventDefault()
        var formData = encodeEntities(typeof formBuilder.data('formBuilder').formData == "string" ? formBuilder.data('formBuilder').formData : xmlToString(formBuilder.data('formBuilder').formData));
        if(idDocument){
            query = "UPDATE typedocument SET nomfichier = '"+formData+"' WHERE idtype = "+idDocument+";";
        }else{
            query = "INSERT INTO typedocument(idtype, nomtype, nomfichier) VALUES (DEFAULT, 'Doc1', '"+formData+"');";
        }
        insertQuery(query, function(data){
            if(data.match(/1/i)){
                notifySucceed("Mise à jour réussie.", "Votre questionnaire a été mis à jour");
            }else{
                notifyFail("Mise à jour échouée.", "Votre questionnaire n'a pas été mis à jour <br><strong>Veuillez réessayer de sauvegarder.</strong>");
            }
            
        })
    });

    $('.edit-form', formContainer).click(function() {
        $fBInstance.toggle();
        $(formContainer).toggle();
    });
}


function loadExistingDocuments(){
    selectQuery("SELECT * FROM typedocument", function(data){
        $.each(data.results, function( index, value ) {
            $(".docSelection").append("<li  data-name='"+value.nomtype+"' class='dash' title='"+value.nomtype+"'><div class='col-xs-6'><span class='nomtype' id='"+value.idtype+"'>"+value.nomtype+"</span></div><div class='col-xs-6'><icon class='glyphicon glyphicon-plus clone' data-clone='"+value.idtype+"' data-toggle='modal' data-target='#cloneDoc' title='cloner ce document'/><icon class='glyphicon glyphicon-remove delete' data-delete='"+value.idtype+"' data-toggle='modal' data-target='#deleteDoc'></icon> <icon class='glyphicon glyphicon-edit edit' data-edit='"+value.idtype+"' data-toggle='modal' data-target='#editDoc'></icon></div></li>")
        });
        $(".docSelection li").removeClass("selected");
        $("#"+idDocument+"").addClass("selected");
        $(".docSelection li:not(.newDocument), .docSelection li:not(.newDocument) span").click(function(e){
            if (e.target !== this)
                return;
            idDocument = $(this).attr("id");
            if(idDocument == undefined)
                idDocument = $(this).parent().attr("id");
            window.location.href = buildUrlWithParams();
        })

        $(".docSelection .edit").click(function(e){
            e.preventDefault();
            var id = $(this).attr("data-edit")
            $("#nomDoc").val($("#"+id).attr("data-name"))
            $("#idDoc").val(id)
        })

        $(".docSelection .delete").click(function(){
            var id = $(this).attr("data-delete")
            $("#idDoc").val(id)
        })

        $(".docSelection .clone").click(function(){
            var id = $(this).attr("data-clone")
            $("#idDoc").val(id)
        })

        fit();
    })

}

function handleAdd(){
    $("#add").click(function(){
        query = "INSERT INTO typedocument(idtype, nomtype, nomfichier) VALUES (DEFAULT, '"+encodeEntities($("#nom").val())+"', '') RETURNING idtype;";
        selectQuery(query, function(data){
            idDocument = data.results[0].idtype
            $("#nom").val("")
            $(".docSelection li:not(.newDocument)").remove();
            loadExistingDocuments();
            window.location.href = buildUrlWithParams();
        })
    })
    $("#commitChanges").click(function(){
        query = "UPDATE typedocument SET nomtype = '"+encodeEntities($("#nomDoc").val())+"' WHERE idtype = "+$("#idDoc").val()+";";
        insertQuery(query, function(data){
            notifySucceed("Nom modifié","Le nom du questionnaire a été modifié.")
            $(".docSelection li:not(.newDocument)").remove();
            loadExistingDocuments();
        })
    })
    $("#delete").click(function(){
        query = "DELETE FROM typedocument WHERE idtype = "+$("#idDoc").val()+";";
        insertQuery(query, function(data){
            data = data.trim();
            if(data == 1){
                notifySucceed("Document supprimé","Le document a été correctement supprimé.")
                $(".docSelection li:not(.newDocument)").remove();
                loadExistingDocuments();
            }else{
                $("#forceDeleteDoc").modal('show');
            }
        })
    })
    $("#clone").click(function(){
        query = "INSERT INTO typedocument(nomtype, nomfichier) SELECT nomtype,nomfichier FROM typedocument WHERE idtype = "+$("#idDoc").val()+" RETURNING idtype;";
        selectQuery(query, function(data){
            idDocument = data.results[0].idtype
            $("#nom").val("")
            $(".docSelection li:not(.newDocument)").remove();
            loadExistingDocuments();
            window.location.href = buildUrlWithParams();
        })
    })
    $("#forceDelete").click(function(){
        queryDeleteQuestionnaires = "DELETE FROM modele_document WHERE idtype = "+$("#idDoc").val()+";";
        queryDeleteModele = "DELETE FROM typedocument WHERE idtype = "+$("#idDoc").val()+";";
        insertQuery(queryDeleteQuestionnaires, function(data){
            data = data.trim();
            if(data == 1){
                insertQuery(queryDeleteModele, function(data){
                    data = data.trim();
                    if(data == 1){
                        notifySucceed("Document supprimé","Le document a été correctement supprimé.")
                        $(".docSelection li:not(.newDocument)").remove();
                        loadExistingDocuments();
                    }else{
                        notifyFail("Erreur inconnue", "Le document n'a pas été correctement supprimé.")
                    }
                })
            }else{
                notifyFail("Erreur inconnue", "Le document n'a pas été correctement supprimé.")
            }
        })
    })
}

function selectQuery(query, success){
    vdata={"dbname":dbname, "user":user, "pw":pw, "query": query};
    $.ajax({
        type: "POST",
        url: '../index.php?type=11',
        contentType: "application/x-www-form-urlencoded;charset=ISO-8859-1",
        data: vdata,
        dataType:"json"
    }).done(function( data ){
        success(data);
    });
}
function insertQuery(query, success){
    vdata={"dbname":dbname, "user":user, "pw":pw,"nomfichier":"Doc1","file":"", "query": query};
    $.ajax({
      type: "POST",
      url: '../index.php?type=20',
      contentType: "application/x-www-form-urlencoded;charset=ISO-8859-1",
      data: vdata
    }).done(function(data){
        success(data);
    });
}

var language = {
        addOption: 'Ajouter une option',
        allFieldsRemoved: 'Vider tous les champs',
        allowSelect: 'Autoriser la selection',
        autocomplete: 'Auto-completion',
        button: 'Boutton',
        cannotBeEmpty: 'Ce champs ne peut pas être vide',
        checkboxGroup: 'Case(s) à cocher',
        checkbox: 'Case à cocher',
        checkboxes: 'Cases à cocher',
        className: 'Nom de la classe CSS',
        clearAllMessage: 'Vider tous les champs',
        clearAll: 'Tout vider',
        close: 'Fermer',
        copy: 'Copier',
        dateField: 'Champs date',
        description: 'Description',
        descriptionField: 'Champs description',
        devMode: 'Mode développeur',
        editNames: 'Editer les noms',
        editorTitle: 'Titre de l\'éditeur',
        editXML: 'Editer le XML',
        fieldDeleteWarning: false,
        fieldVars: 'Variable des champs',
        fieldNonEditable: 'Champs non éditable',
        fieldRemoveWarning: 'Êtes-vous sûr de vouloir supprimer ce champs?',
        fileUpload: 'Téléversement de fichier',
        formUpdated: 'Mise à jour de formulaire',
        getStarted: 'Pour commencer, glissez un élément ici.',
        hide: 'Cacher',
        hidden: 'Caché',
        label: 'Libellé',
        labelEmpty: 'Libellé vide',
        limitRole: 'Limiter au rôle:',
        mandatory: 'Mandatory',
        maxlength: 'Nombre de caractère maximum',
        minOptionMessage: 'Option minimum de message',
        name: 'Nom',
        no: 'Numéro',
        off: 'Désactiver',
        on: 'Activer',
        option: 'Option',
        optional: 'Optionnel',
        optionLabelPlaceholder: 'Libellé du placeholder de l\'option',
        optionValuePlaceholder: 'Libellé du placeholder de la valeur',
        optionEmpty: 'Option vide',
        paragraph: 'Paragraphe',
        placeholders: {
          value: 'Valeur',
          label: 'Libellé',
          text: 'Texte',
          textarea: 'Texte',
          email: 'Email',
          placeholder: 'Placeholder',
          className: 'Classe css',
          password: 'Mot de passe'
        },
        preview: 'Aperçu',
        radioGroup: 'Groupe de boutons radio',
        radio: 'Radio bouton',
        removeMessage: 'Supprimer le message',
        required: 'Enregistrer les données dans les informations du patient.',
        richText: 'Texte riche',
        roles: 'Rôle',
        save: 'Sauvegarder',
        selectOptions: 'Sélectionner une option',
        select: 'Sélectionner',
        selectColor: 'Sélectionner une couleur',
        selectionsMessage: 'Sélectionner un message',
        size: 'Taille',
        style: 'Style',
        styles: {
          btn: {
            'default': 'Par défaut',
            danger: 'Danger',
            info: 'Information',
            primary: 'Primaire',
            success: 'Succés',
            warning: 'Avertissement'
          }
        },
        subtype: 'Sous type',
        subtypes: {
          text: ['text', 'password', 'email', 'color'],
          button: ['button', 'submit']
        },
        text: 'Texte',
        textArea: 'Champs texte',
        toggle: 'Déplier',
        warning: 'Avertissement!',
        viewXML: 'Voir le XML',
        yes: 'Oui',
        no: 'Non',
        header: 'Entête'
    };
function notifySucceed(title, message){
    $("body").append('<div class="alert alert-success" role="alert">'+title+'<br>'+message+'</div>')
    setTimeout(function(){ $(".alert").remove() }, 3000);
}
function notifyFail(title, message){
    $("body").append('<div class="alert alert-danger" role="alert">'+title+'<br>'+message+'</div>')
    setTimeout(function(){ $(".alert").remove() }, 3000);
}
function decodeEntities(encodedString) {
    var textArea = document.createElement('textarea');
    textArea.innerHTML = encodedString;
    return validateXMLAndReturnFixed(textArea.value);
}
function encodeEntities(str){
    //console.log($(str).html())
    return str.replace(/[\u00E0-\u00FC\'\""]/gim, function(i) {
       return '&#'+i.charCodeAt(0)+';';
    });
}
function validateXMLAndReturnFixed(xml){

    xml = xml.replaceAll("&lt;", "");
    xml = xml.replaceAll("&gt;", "");

    regexp = /="[^"]*[<>=(&lt;)]+[^"]*"/g;
    malformed = xml.match(regexp);
    if (malformed) {
        for (var i = malformed.length - 1; i >= 0; i--) {
            var initial = malformed[i]
            var fixed = initial.replaceAll("<", "moins que")
            fixed = fixed.replaceAll(">", "plus que")
            fixed = fixed.replaceAll("=", "");
            fixed = "="+fixed+"";
            xml = xml.replace(initial, fixed);
        }
    }

    regexp = /">.*[<>=]+.*<\//g;
    malformed = xml.match(regexp);
    if (malformed) {
        for (var i = malformed.length - 1; i >= 0; i--) {
            var initial = malformed[i].slice(2, -2);
            var fixed = initial.replaceAll("<", "moins que")
            fixed = fixed.replaceAll(">", "plus que")
            fixed = fixed.replaceAll("=", "");
            fixed = fixed;
            xml = xml.replace(initial, fixed);
        }
    }

    regexp = /="[^"]+["]+[ ]?[^=][^=]+"/g;
    malformed = xml.match(regexp);
    if (malformed) {
        for (var i = malformed.length - 1; i >= 0; i--) {
            var initial = malformed[i]
            var fixed = initial.replaceAll("\"", "")
            fixed = fixed.replaceAll("=", "");
            fixed = "=\""+fixed+"\"";
            xml = xml.replace(initial, fixed);
        }
    }

    xml = xml.replaceAll("&amp;", "");

    return xml;
}
function performUpdateFirstDoc(){
    $.ajax({
        type: "GET",
        url: 'http://rdelaporte.alwaysdata.net/OM/questionnaire/',
        contentType: "application/x-www-form-urlencoded;charset=ISO-8859-15",
        data: vdata,
        dataType:"text"
    }).done(function( data ){
            $("#fb-template").html(decodeEntities(data));
            loadForm();
            query = "UPDATE typedocument SET nomfichier = '"+data+"' WHERE idtype = 1;"
            insertQuery(query, function(data){
            window.location.href = buildUrlWithParams();
        })
    });
}

function reset(){
    $.ajax({
        type: "GET",
        url: 'http://rdelaporte.alwaysdata.net/OM/questionnaire/',
        contentType: "application/x-www-form-urlencoded;charset=ISO-8859-1",
        data: vdata,
        dataType:"text"
    }).done(function( data ){
            query = "DELETE FROM typedocument;"
            insertQuery(query, function(){
                query = "INSERT INTO typedocument(idtype, nomtype, nomfichier) VALUES(1, '" + encodeEntities("Questionnaire médical") + "', '"+data+"');"
                insertQuery(query, function(data){
                    window.location.href = buildUrlWithParams();
                })            
            })
            
    });
}
function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };
function xmlToString(xmlData) { 

    var xmlString;
    //IE
    if (window.ActiveXObject){
        xmlString = xmlData.xml;
    }
    // code for Mozilla, Firefox, Opera, etc.
    else{
        xmlString = (new XMLSerializer()).serializeToString(xmlData);
    }
    return xmlString;
} 

function fit(){
    var p = $('.dash span');
    $('.dash').attr("style", "width:"+$(".docSection").width() - 20+"px");
    var ks = $('.dash').height();
    while ($(p).outerHeight() > ks) {
      $(p).text(function(index, text) {
        return text.replace(/\W*\s(\S)*$/, '...');
      });
    }
}

function reload_js(src) {
        $('script[src="' + src + '"]').remove();
        $('<script>').attr('src', src).appendTo('head');
    }

function buildUrlWithParams(){
    return location.protocol + '//' + location.host + location.pathname + '?db=' + dbname + '&login=' + user + '&pw=' + pw + '&idDocument='+idDocument;
}
