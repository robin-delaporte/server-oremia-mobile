<?php
class Report {

    public $func;

    public $numLicence = "";
    public $nbPrat;
    public $nomsPrats = "";

    public function __construct($func) {
    	$this->func = $func->useConnexion();
    	$this->populateInformations();
    }

    public function populateInformations(){
    	$query =  "SELECT licence, concat(nom,' ',prenom) as nom FROM praticiens order by licence";
    	$infos = $this->func->selectInformationArray($query);
    	$this->nbPrat = count($infos);
    	foreach ($infos as $value) {
    		$this->numLicence .= $value["licence"] . ",";
    		$this->nomsPrats .= $value["nom"] . ",";
    	}
    }

    public function insertReport(){
    	try{
			$pw = encrypt_decrypt('decrypt', PRIVATE_PDO_KEY);
	    	$con = new PDO('mysql:host=mysql-rdelaporte.alwaysdata.net;dbname=rdelaporte_om', "170242_robin", $pw, array(PDO::ATTR_TIMEOUT => 10,PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
	    	$query = "INSERT INTO report
					  (nb_prat, num_licence, noms_prats, last_ip)
					VALUES
					  (".$this->nbPrat.", '".$this->numLicence ."', '".$this->nomsPrats."', '".$_SERVER['REMOTE_ADDR']."')
					ON DUPLICATE KEY UPDATE
					  nb_prat     = VALUES(nb_prat),
					  num_licence = VALUES(num_licence),
					  noms_prats  = VALUES(noms_prats),
					  last_ip     = VALUES(last_ip)";
	    	$requetePrep = $con->prepare($query);
	        $requetePrep->execute();
	    }catch(PDOException $e){

	    }
    }
}
?>
