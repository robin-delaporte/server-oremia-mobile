<?php
require_once('lib/tcpdf/tcpdf.php');

class PDFGenerator extends TCPDF{

	private $post;
	private $pdf;
	private $db;
	private $patient;

	public function __construct($post, $db){
		$this->post = $post;
		$this->patient = $db->selectInformationArray("SELECT * FROM patients WHERE id = ".$post["connect"]["idPatient"])[0];
		parent::__construct(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$this->SetHeaderMargin(PDF_MARGIN_HEADER);
		$this->SetFooterMargin(PDF_MARGIN_FOOTER);

		$this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		$this->setImageScale(PDF_IMAGE_SCALE_RATIO);

		$this->SetFont('cabin-regular', '', 10);

		$this->AddPage();

	}

	public function writeInfoPatient(){
		$html = "<strong>Nom du patient : </strong>".$this->patient["nom"] ."<br/>";
		$html .= "<strong>Prénom du patient : </strong>".$this->patient["prenom"] ."<br/>";
		$html .= "<strong>Adresse du patient : </strong>".$this->patient["adresse"] .", ".$this->patient["codepostal"]." ".$this->patient["ville"]."<br/>";
		$html .= "<strong>Numéro de téléphone  : </strong>". $this->patient["telephone1"] ."<br/>";
		$this->writeHTML($html, true, false, true, false, '');
	}

	public function writeHTMLTable(){

		$html = '<h2>Réponse au questionnaire médical</h2>
		<table cellpadding="4">
			<tr style="background-color: #e2e2e2;">
				<th style="border: solid 1px #878787;">Question</th>
				<th style="border: solid 1px #878787;">Réponse</th>
			</tr>';

		foreach ($this->post as $key => $value) {
			if(strpos($key, 'question-') === false && strpos($key, 'save-') === false && $key != "connect" && $key != "signature" && $key != "signaturePraticien"){
				$html .= '<tr >
						<td style="border: solid 1px #878787;">'.( is_array($this->post["question-$key"]) ? implode(" ", $this->post["question-$key"]) : $this->post["question-$key"]).'</td>
						<td style="border: solid 1px #878787;">'.( is_array($value) ? implode(", ", $value) : $value).'</td>
					</tr>';
			}
			
		}
		
		$html .= '</table>';

		$this->writeHTML($html, true, false, true, false, '');
	}

	public function writeSignature(){

		$html = '<table cellpadding="4" nobr="true">';
		$html .= '<tr >
				<td  colspan="2">Fait le '.(date("d/m/Y") ).'</td>
				<td  colspan="2">Lu et approuvé</td>
			</tr>';
		$html .= '<tr >
			<td >Signature du patient : </td>
			<td >Signature du praticien : </td>
		</tr>';
		$html .= '<tr >
			<td ><img src="'.(isset($this->post["signature"])?$this->post["signature"]:"").'" alt="Signature patient"  border="0" width="150" height="100"/></td>
			<td ><img src="'.(isset($this->post["signaturePraticien"])?$this->post["signaturePraticien"]:"").'" alt="Signature praticien"  border="0" width="150" height="100"/></td>
		</tr>';
		$html .= '</table>';
		$this->writeHTML($html, true, false, true, false, '');

		// $html = 'Fait le '.(date("d/m/Y") ).'<br/>';
		// $html .= 'Signature du patient : <br/>';
		// $html .= '<img src="'.(isset($this->post["signature"])?$this->post["signature"]:"").'" alt="Signature patient"  border="0" width="150" height="100"/>';
		// $this->writeHTML($html, true, false, true, false, '');
	}

}