<?php
require_once("lib/tcpdf/tcpdf.php");
require_once("lib/fpdi/fpdi.php");


class DocumentSigner extends FPDI{
	protected $myPages;
	protected $myPage;
	protected $pdf;
	protected $patient;
	protected $file;
	protected $s;
  	protected $last_page_flag = false;
  	private $sPrat;
  	private $sPatient;
  	private $type;
  	private $pageCount;
  	private $iniPatient;
  	private $iniPraticien;


	public function __construct($file, $sPrat, $sPatient, $type, $idPatient, $idPraticien, $db){
		$this->file = $file;
		$this->sPrat = $sPrat;
		$this->sPatient = $sPatient;
		$this->type = $type;
		$query = $db->getDb()->prepare("SELECT CONCAT(LEFT(prenom,1), LEFT(nom,1)) as inipatient FROM patients WHERE id = $idPatient");
	    $query->execute();
	    $q = $query->fetch();
	    $this->iniPatient = $q["inipatient"];
	    $query = $db->getDb()->prepare("SELECT CONCAT(LEFT(prenom,1), LEFT(nom,1)) as inipraticien FROM praticiens WHERE id = $idPraticien");
	    $query->execute();
	    $q = $query->fetch();
	    $this->iniPraticien = $q["inipraticien"];
		parent::__construct(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$this->SetAutoPageBreak(TRUE, 0);
		$this->SetFont('cabin-regular', '', 7);
		$this->pageCount = $this->setSourceFile( $this->file );
        $this->importPDF();
	}

	private function importPDF(){
		switch ($this->type) {
			case '0':
				$this->importDefault();
				break;
			case '1':
				$this->importWithParafe();
				break;
		}
	}

	private function importDefault(){
		for ($pageNo = 1; $pageNo <= $this->pageCount; $pageNo++) {
            $tplIdx = $this->ImportPage($pageNo);
            $this->s = $this->getTemplatesize($tplIdx);
            $this->AddPage($this->s['w'] > $this->s['h'] ? 'L' : 'P', array($this->s['w'], $this->s['h']));
            $this->useTemplate($tplIdx);
        }
	}

	private function importWithParafe(){
		for ($pageNo = 1; $pageNo <= $this->pageCount; $pageNo++) {
            $tplIdx = $this->ImportPage($pageNo);
            $this->s = $this->getTemplatesize($tplIdx);
            $this->AddPage($this->s['w'] > $this->s['h'] ? 'L' : 'P', array($this->s['w'], $this->s['h']));
            $this->useTemplate($tplIdx);
            if ($pageNo != $this->pageCount) {
            	$this->writeParafes();
            }
            
        }
	}

	private function writeParafes(){
		$html = '<table cellpadding="4" nobr="true">';
		$html .= '<tr >
			<td >'.$this->iniPatient.'</td>
			<td >'.$this->iniPraticien.'</td>
		</tr>';
		$html .= '</table>';
		$this->setY($this->s['h'] - 27);
		$this->setX(125);
		$this->writeHTML($html, true, false, true, false, '');
	}

	public function writeImage(){
		switch ($this->type) {
			case '0':
				$this->writeSignatureDefault();
				break;
			case '1':
				$this->writeSignatureConventionnelle();
				break;
		}
	}

	public function writeSignatureDefault(){
		$html = '<table cellpadding="4" nobr="true">';
		$html .= '<tr >
				<td  colspan="2">Fait le '.(date("d/m/Y") ).'</td>
			</tr>';
		$html .= '<tr >
			<td >Signature du praticien : </td>
			<td >Signature du patient : </td>
		</tr>';
		$html .= '<tr >
			<td >'.($this->sPrat != "" ? '<img src="data:image/png;base64,'.$this->sPrat .'" width="75" height="50"/>' : "").'</td>
			<td >'.($this->sPatient != "" ? '<img src="data:image/png;base64,'.$this->sPatient .'" width="75" height="50"/>' : "").'</td>
		</tr>';
		$html .= '</table>';
		$this->setY($this->s['h'] - 50);
		$this->setX(30);
		$this->writeHTML($html, true, false, true, false, '');
	}

	public function writeSignatureConventionnelle(){
		$html = '<table cellpadding="4" nobr="true">';

		$html .= '<tr >
			<td style="width:50px;color:#151515;">Fait le '.(date("d/m/Y") ).'</td>
			<td style="width:400px;">'.($this->sPatient != "" ? '<img src="data:image/png;base64,'.$this->sPatient .'" width="65" height="40"/>' : "").'</td>
			<td >'.($this->sPrat != "" ? '<img src="data:image/png;base64,'.$this->sPrat .'" width="65" height="40"/>' : "").'</td>
		</tr>';
		$html .= '</table>';
		$this->setY($this->s['h'] - 28);
		$this->setX(30);
		$this->writeHTML($html, true, false, true, false, '');
	}

	public function Close() {
		$this->last_page_flag = true;
		parent::Close();
	}





}