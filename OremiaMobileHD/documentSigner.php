<?php

    // ini_set('display_errors', 1);
    // ini_set('display_startup_errors', 1);
    // error_reporting(E_ALL);
    error_reporting(0);
    include_once 'lib/db_functions.php';
    include_once 'lib/DocumentSigner.php';

    $db = (new DB_Functions())->useConnexion();
    $id = $_GET["idDocument"];
    $filePath = $db->extractDocument($id);
    $query = $db->getDb()->prepare("SELECT nom  FROM documents WHERE id = $id;");
    $query->execute();
    $q = $query->fetch();
    $nom = $q["nom"];
    $pattern = '/[0-9-]*/';
    $nom = preg_replace($pattern, "", $nom)."-signé";
    $idType = (isset($_GET["idType"]) ? $_GET["idType"] : 0);
    $pdf = new DocumentSigner($filePath, $_POST["sPrat"], $_POST["sPatient"], $idType, $_GET["idPatient"], $_GET["idPraticien"],$db);
    $pdf->writeImage();
    $output = $pdf->Output('./input.pdf', 'S');
    $vretour = true;
    $file = fopen("modele/uploads/input.pdf","w");
    if(!fwrite($file,$output)){
        $vretour = false;
    }else {
        $pgdb = (new DB_Functions())->usePgConnexion();

         $pgdb->insertPDF("modele/uploads/input.pdf", $_GET["idPatient"], $_GET["idPraticien"], $nom);

         $query = $db->getDb()->prepare("SELECT MAX(id) as max FROM documents;");
         $query->execute();
         $vretour = $query->fetch();
         $vretour = $vretour["max"];
    }
    fclose($file);
    echo (int)$vretour;
?>