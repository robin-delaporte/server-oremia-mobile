    <?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
error_reporting(0);
register_shutdown_function( "fatal_handler" );
date_default_timezone_set('Europe/Paris');
try{
include_once 'lib/db_functions.php';
include_once 'lib/DocumentSigner.php';
include_once 'lib/CypherFunctions.php';
include_once 'lib/config.php';
//include_once 'lib/PHPWord/bootstrap.php';
include_once('lib/PDFGenerator.php');
switch ($_GET['type']) {
    case 0:
        include_once 'lib/Report.php';
        $func = (new DB_Functions());
        $db   = $func->defineConnexion($_REQUEST["dbname"], $_REQUEST["user"], $_REQUEST["pw"], 1);
        $report = new Report($func);
        $report->insertReport();
        print_r('{"results":[{"connexionBegin" : "kikou"}]}');
        break;
    case 1:
        $query = isset($_POST["query"])?$_POST["query"]:"";
        $func  = (new DB_Functions());
        $db    = $func->useConnexion();
        $query = str_replace("percent", "%", $query);
        //var_dump($query);die;
        print_r($db->selectInformation($query));

        break;
    case 17:
        $UID         = $_POST["UID"];
        $arr         = $_POST["arr"];
        $idPatient   = $_POST["idPatient"];
        $idPraticien = $_POST["idPraticien"];
        $func        = (new DB_Functions());
        $db          = $func->useConnexion();
        print_r($db->insertActes($idPatient, $idPraticien, $UID, $arr));
        break;
    case 2:
        $query = $_POST["query"];
        $func  = (new DB_Functions());
        $db    = $func->defineConnexion($_POST["dbname"], $_POST["user"], $_POST["pw"], 1);
        $query = str_replace("percent", "%", $query);
        print_r($db->selectInformation($query));
        break;
    case 3:
        $id   = $_GET["id"];
        $func = (new DB_Functions());
        $db   = $func->useConnexion();
        header("Content-type: image/jpeg");
        print_r($db->selectImage($id));
        break;
    case 4:
        $id   = $_GET["id"];
        $func = (new DB_Functions());
        $db   = $func->useConnexion();
        header("Content-type: image/jpeg");
        print_r($db->selectImagePreview($id));
        break;
    case 5:
        $id   = $_GET["id"];
        $func = (new DB_Functions());
        $db   = $func->useConnexion();
        header("Content-type: image/jpeg");
        print_r($db->selectRadio($id));
        break;
    case 6:
        $id       = $_GET["id"];
        $func     = (new DB_Functions());
        $db       = $func->useConnexion();
        $doc      = $db->selectDoc($id);
        $filename = "./modele/uploads/" .str_replace("/", "\\/", $id.$doc[0]["nom"]) . "." . $doc[0]["type"];
        $myfile   = fopen($filename, "w");
        fwrite($myfile, $doc[1]);
        fclose($myfile);
        header("Cache-Control: no-store");
        header("Content-Type: application/octet-stream");
        header('Content-Disposition: attachment; filename="' . basename($filename) . '"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($filename));
        ob_clean();
        flush();
        readfile($filename);
        exit();
        break;
    case 7:
        include_once('lib/imageResize.php');
        $func = (new DB_Functions());
        $db   = $func->usePgConnexion();
        $_FILES['htdocs']['tmp_name'];
        print_r($db->insertImage($_FILES['htdocs']['tmp_name'], $_GET["idPatient"], $_GET["idPraticien"]));
        break;
    case 8:
        $func = (new DB_Functions());
        $db   = $func->useConnexion();
        for ($i = 0; $i < 100000; $i++) {
            print_r($db->insertInformation("INSERT INTO patients(
                id, nir, genre, nom, prenom, adresse, codepostal, ville, telephone1,
                telephone2, email, naissance, creation, idpraticien, idphoto,
                info, autorise_sms, correspondant, ipp2, adresse2, patient_par,
                amc, amc_prefs, profession, correspondants, statut, famille,
                tel1_info, tel2_info)
            VALUES (DEFAULT, DEFAULT, 1, 'patient$i', DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT,
                DEFAULT, DEFAULT, '1993-03-12', '1993-03-12', 500, 6,
                '', DEFAULT, 1, DEFAULT, DEFAULT, DEFAULT,
                DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT,
                DEFAULT, DEFAULT);"));
        }
        break;
    case 20:
        $vretour = false;
        $query   = $_POST["query"];
        $func    = (new DB_Functions());
        $db      = $func->defineConnexion($_POST["dbname"], $_POST["user"], $_POST["pw"], 1);
        if ($db->insertInformationWithCallback($query)) {
            $vretour = true;
        }
        echo (int) $vretour;
        break;
    case 9:
        $vretour = false;
        $query   = $_POST["connect"]["query"];
        $func    = (new DB_Functions());
        $db      = $func->defineConnexion($_POST["connect"]["dbname"], $_POST["connect"]["user"], $_POST["connect"]["pw"], 1);
        if ($db->insertInformationWithCallback($query)) {
            $vretour = true;
        }
        $pdf = new PDFGenerator($_POST, $db);
        $pdf->writeInfoPatient();
        $pdf->writeHTMLTable();
        $pdf->writeSignature();
        $output      = $pdf->Output('./input.pdf', 'S');
        $error       = false;
        $files       = array();
        $func        = (new DB_Functions());
        $dbfct       = $func->defineConnexion($_POST["connect"]["dbname"], $_POST["connect"]["user"], $_POST["connect"]["pw"], 0);
        $idPatient   = $_POST["connect"]["idPatient"];
        $idPraticien = $_POST["connect"]["idPraticien"];
        $nom         = $_POST["connect"]["nomfichier"];
        $vretour     = true;
        $file        = fopen("./modele/uploads/input.pdf", "w");
        if (!fwrite($file, $output)) {
            $vretour = false;
        } else {
            fclose($file);
            $dbfct->insertPDF("./modele/uploads/input.pdf", $idPatient, $idPraticien, $nom);
        }

        if ($vretour) {
            $infos = $db->selectInformationArray("SELECT info FROM patients WHERE id = " .$idPatient. ";");
            if (count($infos)) {
                $info = $infos[0]["info"];

                foreach ($_POST as $key => $value) {
                    if(strpos($key, 'question-') === false && strpos($key, 'save-') === false && $key != "connect" && $key != "signature" && $key != "signaturePraticien" && isset($_POST["save-$key"])){
                        $question = ( is_array($_POST["question-$key"]) ? implode(" ", $_POST["question-$key"]) : $_POST["question-$key"] );
                        $reponse = ( is_array($value) ? implode(", ", $value) : $value );
                        $pattern = "/".str_replace("/", "\/",preg_quote( $question)).".*/";
                        if(preg_match($pattern, $info)){
                            $info = preg_replace($pattern, "$question : $reponse", $info);
                        }else{
                            $info = $info . "\n$question : $reponse";
                        }
                    }
                }
                //echo "$info";
                $vretour = $db->insertInformationWithCallbackNoEncoding("UPDATE patients SET info = '".pg_escape_string($info)."' WHERE id = " .$idPatient. ";");
            }
        }

        echo (int) $vretour;
        // print_r($vretour);
        break;
    case 10:
        
        $error = false;
        $files = array();
        
        $uploaddir = './modele/uploads/';
        foreach ($_FILES as $file) {
            chmod("./modele/uploads/", 0777);
            
            if (move_uploaded_file($file['tmp_name'], $uploaddir . basename($file['name']))) {
                $func = (new DB_Functions());
                print_r($func->cloudConvert($uploaddir . $file['name']));
            } else {
                $error = true;
                echo $error;
            }
        }
        
        break;
    case 11:
        $query = $_POST["query"];
        if (isset($_POST["pw"])) {
            $pw = $_POST["pw"];
        }else{
            $pw = "zuma";
        }
        
        $func  = (new DB_Functions());
        $db    = $func->defineConnexion($_POST["dbname"], $_POST["user"], $pw, 1);
        if (isset($_POST["type"])) {
            print_r($db->insertUT8Information($query));
        }
        //  print_r($query);die;
        print_r($db->selectUT8Information($query));
        break;
    case 12:
        $error = false;
        $files = array();
        
        $uploaddir = './modele/uploads/';
        foreach ($_FILES as $file) {
            if (move_uploaded_file($file['tmp_name'], $uploaddir . basename($file['name']))) {
                print_r($error);
            } else {
                $error = true;
                echo $error;
            }
        }
        break;
    case 13:
        $error       = false;
        $files       = array();
        $func        = (new DB_Functions());
        $dbfct       = $func->usePgConnexion();
        $idPatient   = $_POST["idPatient"];
        $idPraticien = $_POST["idPraticien"];
        $nom         = $_POST["nomfichier"];
        $vretour     = true;
        $file        = fopen("./modele/uploads/input.html", "w");
        if (!fwrite($file, $_POST["html"])) {
            $vretour = false;
        } else {
            fclose($file);
            $url = $dbfct->cloudConvertPDF("./modele/uploads/input.html");
            $dbfct->insertPDF($url, $idPatient, $idPraticien, $nom);
        }
        echo $vretour;
        break;
    case 14:
        $func    = (new DB_Functions());
        $db      = $func->useConnexion();
        // $query = "SELECT inifile FROM config WHERE nom='ccam_favoris'";
        $query   = $_POST["query"];
        $vretour = $db->selectInifile($query);
        $array   = array();
        $arr = $db->parse_ini_string_m(is_array($vretour)&&isset($vretour[0])?$vretour[0]["inifile"]:array());
        foreach (is_array($arr)? $arr : $array as $key => $value) {
            $value["nom"] = $key;
            if ($key != "Entete")
                $array[] = $value;
        }
        
        print_r($db->createJSON($array));
        break;
    case 15:
        $id       = $_POST["id"];
        $func     = (new DB_Functions());
        $db       = $func->useConnexion();
        $doc      = $db->selectDoc($id);
        $filename = "./modele/uploads/" . $doc[0]["nom"] . "." . $doc[0]["type"];
        $myfile   = fopen($filename, "w");
        fwrite($myfile, $doc[1]);
        fclose($myfile);
        $func = (new DB_Functions());
        print_r($func->cloudConvert($filename));
        break;
    case 16:
        $db = new DB_Functions();
        $db->readWordFile();
        break;
    case 21:
        $phpWord = \PhpOffice\PhpWord\IOFactory::load('text.docx');
        $phpWord->save("doc.docx");
        
        
        break;
}


}catch(PDOException $e){
    echo '{"results":[{"error":'.$e->getCode().'}]}';
}

function fatal_handler() {
    // header('Content-Type:text/plain'); 
    include_once 'lib/Report.php';

    $errfile = "unknown file";
    $errstr  = "shutdown";
    $errno   = E_CORE_ERROR;
    $errline = 0;
    $error = error_get_last();
    if( $error !== NULL) {
        if($error['type'] === E_ERROR){
            http_response_code(500);
        }
        try{
            $pw = encrypt_decrypt('decrypt', PRIVATE_PDO_KEY);
            $errno   = addslashes($error["type"]);
            $errfile = addslashes($error["file"]);
            $errline = addslashes($error["line"]);
            $errstr  = addslashes($error["message"]);
            $con = new PDO('mysql:host=mysql-rdelaporte.alwaysdata.net;dbname=rdelaporte_om', "170242_robin", $pw);
            $query = "INSERT INTO bug_tracker
                      (type, file, line, message)
                    VALUES
                      ('".$errno."', '".$errfile ."', '".$errline."', '".$errstr."')
                    ON DUPLICATE KEY UPDATE
                      type     = VALUES(type)";
            $requetePrep = $con->prepare($query);
            $requetePrep->execute();


            $func = (new DB_Functions());
            $report = new Report($func);

        }catch(PDOException $e){
            //var_dump($e);
        }
       
    }

}

function pinfo() {
    ob_start();
    phpinfo();
    $data = ob_get_contents();
    ob_clean();
    return $data;
}
?>
