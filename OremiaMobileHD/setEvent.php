<?php
include_once 'lib/db_functions.php';
include_once 'lib/config.php';
include_once 'lib/CypherFunctions.php';
require_once('lib/CalDav/SimpleCalDAVClient.php');
register_shutdown_function( "fatal_handler" );
date_default_timezone_set('Europe/Paris');
error_reporting(0);
$client = new SimpleCalDAVClient();
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
try {
	/*
	 * To establish a connection and to choose a calendar on the server, use
	 * connect()
	 * findCalendars()
	 * setCalendar()
	 */
	 $idP = $_GET["idP"];
	 $db = (new DB_Functions())->useConnexion();
	 $infos = $db->getCalDAVInfos($idP);
	 if(count($infos)==3){
		 // print_r($properUrl);
		 $client->connect('https://localhost:8443'.$infos[2], $infos[0], $infos[1]);

		$calTitle = $_GET['TITLE'];
		$client->setCalendar(new CalDavCalendar($infos[2])); 
	    $UID = str_replace("@oremia.com", "", $_GET['UID']);

	  $IPP = $_GET['IPP'];
	  $statut = $_GET['STATUT'];
		$type = $_GET['TYPE'];
		$dtstart = "DTSTART:".$_GET['DTSTART'];
		$dtend = "DTEND:".$_GET['DTEND'];
		$summary = 'SUMMARY:'.$_GET['SUMMARY'];
		$note = $_GET['NOTES'];
	  $ressources = "RESOURCES;X-ORE-IPP=%".$IPP."%";
	  if(isset($statut)){
	    $ressources .= ";X-ORE-STATUT=%".$statut."%";
	  }
		if(isset($type)){
	    $ressources .= ";X-ORE-TYPE=%".$type."%";
	  }
		$ressources .= ":";


		$date = DateTime::createFromFormat("Y-m-d", $_GET['date']);
		$interval = new DateInterval("P1D"); // 4 months
		$dateDeb = date_format($date->sub($interval),"Ymd")."T000000Z";
		$interval = new DateInterval("P2D");
		$dateFin = date_format($date->add($interval),"Ymd")."T000000Z";
		$events = $client->getEvents($dateDeb, $dateFin); // Returns array($secondNewEventOnServer);

		// echo $events[0]->getData(); // Prints $secondNewEvent. See CalDAVObject.php

		// $client->delete($secondNewEventOnServer->getHref(), $secondNewEventOnServer->getEtag()); // Deletes the second new event from the server.

		// $client->getEvents('20140418T103000Z', '20140419T200000Z'); // Returns an empty array


		// echo '<pre>';
		// var_dump($events);
		// echo '</pre>';

	    /*
	     * You can create custom queries to the server via CalDAVFilter. See CalDAVFilter.php
	     */

	    $filter = new CalDAVFilter("VEVENT");
	    $filter->mustIncludeMatchSubstr("UID", $UID, FALSE);
	    $events = $client->getCustomReport($filter->toXML());
		// echo "<pre>";
		// print_r($events);
		// echo "</pre>";
		if (count($events) == 0){
			$evt = array($client->createEvent($UID,$dtstart,$dtend,$summary,$note,$ressources));

		}else {
		    $data = $events[0]->getData();
				$arraydata = explode("\n",$data);
				$data = "";
				$found = false;
				foreach ($arraydata as $key) {
					if(count(explode("RESOURCES",$key))>1){
						$found = true;
						$key = $ressources;
					}else if(count(explode("SUMMARY",$key))>1){
						if(!$found ){
							$key = $ressources."\n".$summary;
						}else {
							$key = $summary;
						}
					}else if(count(explode("DTSTART",$key))>1){
						$key = $dtstart;
					}else if(count(explode("DTEND",$key))>1){
						$key = $dtend;
					}
					$data .= $key."\n";
				}
				//  var_dump($data);
		    $evt = array($client->change($events[0]->getHref(),$data, $events[0]->getEtag()));
			}
			print_r( '{"results":['.$client->getClientEvents($evt).']}');
	}
}

catch (Exception $e) {
	echo $e->__toString();
}

function fatal_handler() {
    // header('Content-Type:text/plain'); 


    $errfile = "unknown file";
    $errstr  = "shutdown";
    $errno   = E_CORE_ERROR;
    $errline = 0;
    $error = error_get_last();
    if( $error !== NULL) {
        if($error['type'] === E_ERROR){
            http_response_code(500);
        }
        try{
            $pw = encrypt_decrypt('decrypt', PRIVATE_PDO_KEY);
            $errno   = addslashes($error["type"]);
            $errfile = addslashes($error["file"]);
            $errline = addslashes($error["line"]);
            $errstr  = addslashes($error["message"]);
            $con = new PDO('mysql:host=mysql-rdelaporte.alwaysdata.net;dbname=rdelaporte_om', "170242_robin", $pw);
            $query = "INSERT INTO bug_tracker
                      (type, file, line, message)
                    VALUES
                      ('".$errno."', '".$errfile ."', '".$errline."', '".$errstr."')
                    ON DUPLICATE KEY UPDATE
                      type     = VALUES(type)";
            $requetePrep = $con->prepare($query);
            $requetePrep->execute();


            $func = (new DB_Functions());
            $report = new Report($func);

        }catch(PDOException $e){
            //var_dump($e);
        }
       
    }

}

function pinfo() {
    ob_start();
    phpinfo();
    $data = ob_get_contents();
    ob_clean();
    return $data;
}

?>
